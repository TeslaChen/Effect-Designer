// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	EDResSelectDlg.h 
// File mark:   
// File summary:添加美妆或者贴纸弹出的资源选择对话框
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-6-12
// ----------------------------------------------------------------
#pragma once

int Show_ResSelectDlg(LPCWSTR lpResTag, EDJSonParser* pJsonParser, HWND hWnd);
class EDResSelectDlg : public DMHDialog
{
public:
	EDResSelectDlg();

private:
	DECLARE_MESSAGE_MAP()
	DECLARE_EVENT_MAP()
	BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam);
	void OnSize(UINT nType, CSize size);
	DMCode OnBtnClick(int uID);
	DMCode OnBtnClose();
	DMCode OnBtnOk();
	DMCode OnBtnImportImg();
public:
	EDJSonParser*					m_pJsonParser;
	CStringW                        m_strTag;
};