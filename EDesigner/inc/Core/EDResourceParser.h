//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDResourceParser.h 
// File Des: resource字段  Json解析类
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-5-25	1.0		
//-------------------------------------------------------
#pragma once

namespace EDAttr
{
	class EDResourceNodeParserAttr
	{
	public:
		static char* STRING_imagePath;                          ///< 示例:"imagePath" : "__facemorph/eyeC.png"
		static char* STRING_name;								///< 示例:"name":"eyeC"
		static char* STRING_tag;								///< 示例:"tag" : "EYE";  "tag" : "2DSticker"
		static char* INT_PreviewPngIndex;						///< 示例:"INT_PreviewPngIndex" : 0
	};
	EDAttrValueInit(EDResourceNodeParserAttr, STRING_imagePath)EDAttrValueInit(EDResourceNodeParserAttr, STRING_name)EDAttrValueInit(EDResourceNodeParserAttr, STRING_tag)EDAttrValueInit(EDResourceNodeParserAttr, INT_PreviewPngIndex)
}

/// <summary>
///		"resource"节点json字段解析类
/// </summary>
class EDResourceNodeParser;
class EDResourceParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDResourceParser, L"EDResource", DMREG_Attribute);
public:
	EDResourceParser();
	~EDResourceParser();

	static EDResourceParser* GetResourceParserIns(EDJSonParser* pParent = NULL);
	static bool IsResourceNameCharacterValid(const CStringW& strNewName, const CStringW& strTag, bool bMutiFiles, CStringW& strOutTip);					///< 命名规则检查
	static bool IsNameOnlyContainNumLettersUnderline(const CStringW& strName);				///< 只包含数字、字母、下划线
	bool	IsResourceNameSameConflict(EDResourceNodeParser* pParser, CStringW strNewName); ///< 重名冲突
	bool	IsResourceNameSameConflict(const CStringW& strTag, const CStringW& strNewName); ///< 重名冲突
	CStringW GetAppdataTmpDir();
	EDResourceNodeParser* IsResourceNodeExist(CStringW strimagePath, CStringW strName, CStringW strResTag);
	EDResourceNodeParser* InsertNewResourceNode(CStringW strimagePath, CStringW strName, CStringW strResTag);

	DMCode InitJSonData(JSHandle &JSonHandler) override;
	DMCode BuildJSonData(JSHandle JSonHandler) override;

public:
	static EDResourceParser* sta_pResourceParser;
private:
	CStringW m_strTempDir;
};

/// <summary>
///		"resource"每个子节点json字段解析类
/// </summary>
class EDResourceNodeParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDResourceNodeParser, L"EDResourceNode", DMREG_Attribute);
public:
	EDResourceNodeParser();
	~EDResourceNodeParser();

	DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;
	DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;
	void   FreeParser(EDJSonParser* pParser) override;
	DMCode SetCurResPreviewPngIndex(int iIndex);
	DMCode RenameResource(const CStringW& strNewName);
	DMCode RemoveProjectUnuseImgRes();
	DMCode MoveProjectDirImgRes(CStringW strNewDest);
	DMCode MoveBuildResourceFiles();

	VOID	LoadImgFromPath();
	VOID	BindCurResourceParserOwner(EDJSonParser* pParser);
	VOID	GetImgSize(INT& uiWidth, INT& uiHeight);
public:
	CStringW		m_strImagePath;
	CStringW		m_strName;
	CStringW		m_strResTag;
	int				m_iCurResPreviewPngIndex;
public:
	int				m_iFrameCount;
	CStringWList	m_ArrImageList;
 	DMSmartPtrT<IDMBitmap> m_pBmp;
	EDJSonParser*	m_pResourceParserOwner; ///当前资源被什么节点所拥有 比如被makeupnode或者partnode所拥有
};