//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDTransitionParser.h 
// File Des: Transition字段  Json解析类
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-5-30	1.0		
//-------------------------------------------------------
#pragma once

namespace EDAttr
{
	class EDTransitionsNodeParserAttr
	{
	public:
		static char* BOOL_randomShow;									///< 示例:"randomShow" : true
	};
	EDAttrValueInit(EDTransitionsNodeParserAttr, BOOL_randomShow)

	class EDConditionsNodeParserAttr
	{
	public:
		static char* STRING_preState;									///< 示例:"preState" : "channel.last_frame"
		static char* STRING_triggers;									///< 示例:"preState" : "H.face.appear"
	};
	EDAttrValueInit(EDConditionsNodeParserAttr, STRING_preState)EDAttrValueInit(EDConditionsNodeParserAttr, STRING_triggers)

	class EDTargetsNodeParserAttr
	{
	public:
		static char* INT_delay;											///< 示例:"delay" : 10
		static char* INT_fadingFrame;									///< 示例:"fadingFrame" : 0
		static char* INT_lastingFrame;									///< 示例:"lastingFrame" : 20
		static char* INT_loop;											///< 示例:"loop" : 0
		static char* STRING_targetPart;									///< 示例:"targetPart" : "tiezhi"
		static char* STRING_targetState;								///< 示例:"targetState" : "paused"
	};
	EDAttrValueInit(EDTargetsNodeParserAttr, INT_delay)EDAttrValueInit(EDTargetsNodeParserAttr, INT_fadingFrame)EDAttrValueInit(EDTargetsNodeParserAttr, INT_lastingFrame)
	EDAttrValueInit(EDTargetsNodeParserAttr, INT_loop)EDAttrValueInit(EDTargetsNodeParserAttr, STRING_targetPart)EDAttrValueInit(EDTargetsNodeParserAttr, STRING_targetState)
}

/// <summary>
///		"Transition"节点json字段解析类
/// </summary>
class EDTransitionsParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDTransitionsParser, L"transitions", DMREG_Attribute);
public:
	EDTransitionsParser();
	~EDTransitionsParser();

	DMCode BuildJSonData(JSHandle JSonHandler) override;
	DMCode ParseMemberJSObj(LPCSTR pszAttribute, JSHandle& JSonHandler, bool bLoadJSon) override;
};

/// <summary>
///		"Transition"子节点json字段解析类
/// </summary>
class EDTransitionsNodeParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDTransitionsNodeParser, L"TransitionsNode", DMREG_Attribute);
public:
	EDTransitionsNodeParser();
	~EDTransitionsNodeParser();

	DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;
	DMCode BuildJSonData(JSHandle JSonHandler) override;
	DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;

public:
	bool			m_bRandomShow;
};

/// <summary>
///		"conditions"节点json字段解析类
/// </summary>
class EDConditionsParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDConditionsParser, L"conditions", DMREG_Attribute);
public:
	EDConditionsParser();
	~EDConditionsParser();

	DMCode InitJSonData(JSHandle &JSonHandler) override;
	DMCode BuildJSonData(JSHandle JSonHandler) override;
};

/// <summary>
///		"conditions"每个子节点json字段解析类
/// </summary>
class EDConditionsNodeParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDMakeupsNodeParser, L"conditionsNode", DMREG_Attribute);
public:
	EDConditionsNodeParser();
	~EDConditionsNodeParser();

	DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;
	DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;
public:
	CStringW		m_strPreState;
	CStringW		m_strTriggers;

	//没有写入到json文件  树控件是否展开
public:
	bool			m_bTreeItemCollapsed;//折叠状态
};

/// <summary>
///		"targets"节点json字段解析类
/// </summary>
class EDTargetsParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDTargetsParser, L"targets", DMREG_Attribute);
public:
	EDTargetsParser();
	~EDTargetsParser();

	DMCode InitJSonData(JSHandle &JSonHandler) override;
	DMCode BuildJSonData(JSHandle JSonHandler) override;
};

/// <summary>
///		"EDTargetsParser"每个子节点json字段解析类
/// </summary>
class EDTargetsNodeParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDMakeupsNodeParser, L"targetsNode", DMREG_Attribute);
public:
	EDTargetsNodeParser();
	~EDTargetsNodeParser();

	DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;
	DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;
public:
	int				m_iDelay;
	int				m_iFadingFrame;
	int				m_iLastingFrame;
	int				m_iLoop;
	CStringW		m_strTargetPart;
	CStringW		m_strTargetState;

	//没有写入到json文件  树控件是否展开
public:
	bool			m_bTreeItemCollapsed;//折叠状态
};
