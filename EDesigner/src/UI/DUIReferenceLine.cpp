#include "StdAfx.h"
#include "DUIReferenceLine.h"

DUIReferenceLine::DUIReferenceLine()
{
	memset(m_point, 0, sizeof(m_point));
	m_Clr = PBGRA(0, 0xff, 0xff, 0xff);
	m_BlackClr = PBGRA(0, 0, 0, 0xff);
}

DUIReferenceLine::~DUIReferenceLine()
{
}

void DUIReferenceLine::SetDotPoint(POINT pt1, POINT pt2)
{
	m_point[0] = pt1;
	m_point[1] = pt2;
}

void DUIReferenceLine::DM_OnPaint(IDMCanvas* pCanvas)
{
	AutoDrawDotLine(pCanvas, m_Clr, PS_DOT, 1, m_point, 2);

	CRect rt1 = { m_point[0].x - 3, m_point[0].y - 3, m_point[0].x + 3, m_point[0].y + 3 };
	CRect rt2 = { m_point[1].x - 3, m_point[1].y - 3, m_point[1].x + 3, m_point[1].y + 3 };
	AutoFillRoundRect(pCanvas, m_Clr, rt1, CPoint(0, 0));
	AutoFillRoundRect(pCanvas, m_Clr, rt2, CPoint(0, 0));
	AutoDrawRoundRect(pCanvas, m_BlackClr, PS_SOLID, 1, rt1, CPoint(0, 0));
	AutoDrawRoundRect(pCanvas, m_BlackClr, PS_SOLID, 1, rt2, CPoint(0, 0));
}