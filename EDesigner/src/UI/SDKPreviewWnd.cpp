#include "stdafx.h"
#include "SDKPreviewWnd.h"

static UINT FRAME_TIME = 1000 / 25;
static int  VIEW_WIDTH = 216;
static bool MAIN_THREAD_CVBO = true;
static int  VIEW_HEIGHT = 384;
static int  WIDTH = 216;
static int  HEIGHT = 384;
static HGLRC g_Context = NULL;
static HDC	 g_hHDC = NULL;
static int	 g_contextversion = 2;

//app����ע��
#define APP_NAME "likeapp"

BEGIN_MSG_MAP(SDKPreviewWnd)
	MSG_WM_INITDIALOG(OnInitDialog)
	MSG_WM_SIZE(OnSize)
	CHAIN_MSG_MAP(DMHWnd)
END_MSG_MAP()

SDKPreviewWnd::SDKPreviewWnd()
	:m_exit(false)
{
}

SDKPreviewWnd::~SDKPreviewWnd()
{
	uninit();
}

BOOL SDKPreviewWnd::OnInitDialog(HWND wndFocus, LPARAM lInitParam)
{
	// todo: ��ʱ�ر�
	return FALSE;
}

void SDKPreviewWnd::OnDUITimer(char id)
{
	DMHWnd::OnDUITimer(id);
	
}

void SDKPreviewWnd::OnSize(UINT nType, CSize size)
{
	__super::OnSize(nType, size);

	SetMsgHandled(TRUE);

}

void SDKPreviewWnd::setResourcePath(const CStringW& resPath)
{
	std::unique_lock <std::mutex> lck(m_resource_mtx);

	std::string tmpPath = EDBASE::w2a((LPCWSTR)resPath);
	std::replace(tmpPath.begin(), tmpPath.end(), '\\', '/');
	m_resPath = tmpPath;
	m_resource_cv.notify_all();
}

void SDKPreviewWnd::uninit()
{
	m_exit = true;
	std::unique_lock <std::mutex> lck(m_resource_mtx);
	m_resource_cv.notify_all();

}

void SDKPreviewWnd::updatePreview()
{

}

void SDKPreviewWnd::previewPause()
{

}

void SDKPreviewWnd::previewResume()
{

}

void SDKPreviewWnd::setDebugPointShow(bool bShow)
{

}
