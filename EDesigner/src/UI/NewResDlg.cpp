#include "stdafx.h"
#include "NewResDlg.h"
#include "FileHelper.h"

DUIRecentListBox::DUIRecentListBox()
{
	DMADDEVENT(DUIRecentListBoxDBLClickArgs::EventID);
}

void DUIRecentListBox::OnLButtonDbClick(UINT nFlags,CPoint pt)
{
	DUIListBox::OnLButtonDown(nFlags,pt);
	if (-1 != m_iSelItem)
	{
		CStringW strDir = m_DMArray[m_iSelItem]->strText;
		if (!IsDirectoryExist(strDir))
		{
			ED_MessageBox(L"该路径已无效!",MB_OK, L"提示", GetContainer()->OnGetHWnd());
			DeleteString(m_iSelItem);
			g_pCtrlXml->RemoveRecentResDir(strDir);
		}
		else
		{
			DUIRecentListBoxDBLClickArgs Evt(this);
			Evt.m_strDir = strDir;
			DV_FireEvent(Evt);
		}
	}
}

void DUIRecentListBox::OnKeyDown(TCHAR nChar, UINT nRepCnt, UINT nFlags)
{
	if (VK_DELETE == nChar && GetCurSel() > -1)
	{
		CStringW strText;
		GetText(GetCurSel(), strText);
		g_pCtrlXml->RemoveRecentResDir(strText + L"\\");
		DeleteString(GetCurSel());
	}
	SetMsgHandled(FALSE);
}

BEGIN_MSG_MAP(NewResDlg)
	MSG_WM_INITDIALOG(OnInitDialog) 
	CHAIN_MSG_MAP(DMHDialog)
END_MSG_MAP()
BEGIN_EVENT_MAP(NewResDlg)
	EVENT_NAME_COMMAND(L"ds_openrespath",OnOpenResPath)
	EVENT_NAME_HANDLER(L"ds_recentlist",DMEVT_DRLISTBOX_DBLCLICK,OnOpenRecentDir)
END_EVENT_MAP()

BOOL NewResDlg::OnInitDialog(HWND wndFocus, LPARAM lInitParam)
{
	m_pResNameEdit = FindChildByNameT<DUIEdit>(L"ds_resnameedit");DMASSERT(m_pResNameEdit);
	m_pResPathEdit = FindChildByNameT<DUIEdit>(L"ds_respathedit");DMASSERT(m_pResPathEdit);
	m_pRecentList  = FindChildByNameT<DUIListBox>(L"ds_recentlist");DMASSERT(m_pRecentList);
//	m_pResNameEdit->DV_SetFocusWnd();

	// 更新最近list列表
	DMXmlNode XmlRoot = g_pCtrlXml->m_RecentDoc.Root();
	if (XmlRoot.IsValid())
	{
		DMXmlNode XmlNode = XmlRoot.FirstChild();
		while (XmlNode.IsValid())
		{
			CStringW strDir = XmlNode.Attribute(XML_PATH);
			if (!strDir.IsEmpty())
			{
				if (strDir.Right(1) == L'\\')
					m_pRecentList->AddString(strDir.Left(strDir.GetLength() - 1));
				else
					m_pRecentList->AddString(strDir);
			}
	
			XmlNode = XmlNode.NextSibling();
		}
	}

	return TRUE;
}

DMCode NewResDlg::OnOpenResPath()
{ 
	CStringW strSelectFolder;
	SelectFolderPath(strSelectFolder);
	if (!strSelectFolder.IsEmpty())
		m_pResPathEdit->SetWindowText(strSelectFolder);
#if 0
	wchar_t path[MAX_PATH] = {0};
	BROWSEINFOW bi = {0};
	bi.ulFlags = BIF_STATUSTEXT| BIF_RETURNONLYFSDIRS|BIF_VALIDATE;
	bi.lpszTitle = L"请选择DM资源包的存放位置（目前仅支持文件夹方式）";
	bi.hwndOwner = m_hWnd;//设置拥有窗口
	bi.lpfn = NULL;	//指定回调函数地址
	ITEMIDLIST *pIDL = SHBrowseForFolderW(&bi);
	if (pIDL!=NULL)
	{
		if (SHGetPathFromIDListW(pIDL,path) == TRUE) //变量path中存储了经过用户选择后的目录的完整路径.
		{
			m_pResPathEdit->SetWindowText(path);
		}
		// free memory used   
		IMalloc * imalloc = 0;   
		if (SUCCEEDED(SHGetMalloc( &imalloc)))   
		{   
			imalloc->Free(pIDL);   
			imalloc->Release();   
		}   
	}
#endif
	return DM_ECODE_OK;
}

DMCode NewResDlg::OnOpenRecentDir(DMEventArgs *pEvt)
{
	DUIRecentListBoxDBLClickArgs* pEvent = (DUIRecentListBoxDBLClickArgs*)pEvt;
	CStringW strDir = pEvent->m_strDir;

	if (g_pMainWnd)
	{
		CStringW strNewDir = g_pMainWnd->m_strProjectName;
		if (strNewDir.Right(1) == L'\\')
		{
			strNewDir = strNewDir.Left(strNewDir.GetLength() - 1);
		}
		if (strDir.Right(1) == L'\\')
		{
			strDir = strNewDir.Left(strDir.GetLength() - 1);
		}
// 		if (0 == strNewDir.CompareNoCase(strDir))
// 		{
// 			ED_MessageBox(L"当前项目已在编辑当中!", MB_OK, L"提示", m_hWnd);
// 			return DM_ECODE_OK;
// 		}
		m_strResDir = strDir;
		EndDialog(IDRETRY);
	}

	return DM_ECODE_OK;
}

DMCode NewResDlg::OnOK()
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		m_strProjName = m_pResNameEdit->GetWindowText();
		m_strProjPath = m_pResPathEdit->GetWindowText();

		if (m_strProjPath.IsEmpty())
		{
			ED_MessageBox(L"请设置项目目录!", MB_OK, L"提示", m_hWnd);
			break;
		}

		m_strResDir = L"";
		if (m_strProjPath.Right(1)!=L"\\")
		{
			m_strProjPath += L"\\";
		}
		m_strResDir = m_strProjPath + m_strProjName;
		if (!FileHelper::CreateMultipleDirectory((LPCWSTR)m_strResDir))
		{
			ED_MessageBox(L"创建项目目录失败，请检查填写的路径是否规范!", MB_OK, L"提示", m_hWnd);
			break;
		}

		iErr = DM_ECODE_OK;
		EndDialog(IDOK);
	} while (false);
	if (!DMSUCCEEDED(iErr))
	{
		m_strProjName = m_strProjPath = m_strResDir = L"";
	}

	return iErr;
}
