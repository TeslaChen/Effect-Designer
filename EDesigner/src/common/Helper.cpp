#include "stdAfx.h"
#include "Helper.h"
//DMLazyT<DMCtrlXml> g_pAttr;

class KeepCurDirHelper
{
public:
	KeepCurDirHelper()
	{
		GetCurrentDirectory(MAX_PATH, szPath);
		if(!SetCurrentDirectory(_T("C:\\")))
			if(!SetCurrentDirectory(_T("D:\\")))
				if(!SetCurrentDirectory(_T("E:\\")))
					SetCurrentDirectory((LPCWSTR)GetProgramDirectory());
	}
	~KeepCurDirHelper()
	{
		SetCurrentDirectory(szPath);
	}

private:
	TCHAR szPath[MAX_PATH];
};


bool IsDirectoryExist(CStringW strDir)
{
	bool bRet = false;
	do 
	{
		KeepCurDirHelper autoKeepCurDir;
		DWORD dwAttr = GetFileAttributesW(strDir);

		if (INVALID_FILE_ATTRIBUTES != dwAttr
			&& FILE_ATTRIBUTE_DIRECTORY == (dwAttr & FILE_ATTRIBUTE_DIRECTORY))
		{
			bRet = true;
		}
	} while (false);
	return bRet;
}


bool IsFileExist(CStringW strFilePath)
{
	bool bRet = false;
	do
	{
		KeepCurDirHelper autoKeepCurDir;
		DWORD dwAttr = GetFileAttributesW(strFilePath);

		if (INVALID_FILE_ATTRIBUTES != dwAttr
			&& FILE_ATTRIBUTE_DIRECTORY != (dwAttr & FILE_ATTRIBUTE_DIRECTORY))
		{
			bRet = true;
		}
	} while (false);
	return bRet;
}

bool RemoveDir(LPCWSTR szFileDir)
{
	std::wstring strDir = szFileDir;
	if (strDir.at(strDir.length() - 1) != L'\\')
		strDir += L'\\';
	WIN32_FIND_DATA wfd;
	HANDLE hFind = FindFirstFile((strDir + L"*.*").c_str(), &wfd);
	if (hFind == INVALID_HANDLE_VALUE)
		return false;
	do
	{
		if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if (_wcsicmp(wfd.cFileName, L".") != 0 && _wcsicmp(wfd.cFileName, L"..") != 0)
				RemoveDir((strDir + wfd.cFileName).c_str());
		}
		else
		{
			DeleteFile((strDir + wfd.cFileName).c_str());
		}
	} while (FindNextFile(hFind, &wfd));
	FindClose(hFind);
	RemoveDirectory(szFileDir);
	return true;
}

bool CopyDir(LPCWSTR szFileSrcDir, LPCWSTR szFileDestDir)
{
	std::wstring strDir = szFileSrcDir;
	std::wstring strDestDir = szFileDestDir;
	if (strDir.at(strDir.length() - 1) != L'\\' && strDir.at(strDir.length() - 1) != L'/')
		strDir += L'\\';
	if (strDestDir.at(strDestDir.length() - 1) != L'\\' && strDestDir.at(strDestDir.length() - 1) != L'/')
		strDestDir += L'\\';
	WIN32_FIND_DATA wfd;
	HANDLE hFind = FindFirstFile((strDir + L"*.*").c_str(), &wfd);
	if (hFind == INVALID_HANDLE_VALUE)
		return false;
	do
	{
		if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
		}
		else
		{
			CopyFile((strDir + wfd.cFileName).c_str(), (strDestDir + wfd.cFileName).c_str(), FALSE);
		}
	} while (FindNextFile(hFind, &wfd));
	FindClose(hFind);
	return true;
}

CStringW GetAppdataPath()
{
	WCHAR szbufPath[MAX_PATH];
	memset(szbufPath, 0, MAX_PATH*sizeof(WCHAR));
	if (SUCCEEDED(SHGetFolderPathW(NULL,
		CSIDL_APPDATA | CSIDL_FLAG_CREATE,
		NULL,
		0,
		szbufPath)))
	{
		return szbufPath;
	}
	return L"";
}

bool OpenFolderAndSelectFile(CStringW strFilePath)
{
	bool bRet = false;
	::CoInitialize(NULL);
	LPSHELLFOLDER pDesktopFolder = NULL;
	do 
	{
		if (FAILED(SHGetDesktopFolder(&pDesktopFolder)))
		{
			break;
		}

		LPITEMIDLIST pidl;
		ULONG chEaten;
		wchar_t wfilePath[MAX_PATH+1] = {0};
		// IShellFolder::ParseDisplayName要传入宽字节
		wcscpy_s(wfilePath,MAX_PATH+1,strFilePath);

		LPWSTR lpWStr = wfilePath;
		HRESULT hr = pDesktopFolder->ParseDisplayName(NULL, 0, lpWStr, &chEaten, &pidl, NULL);
		if (FAILED(hr))
		{
			break;
		}

		LPCITEMIDLIST cpidl = pidl;
		// SHOpenFolderAndSelectItems是非公开的API函数，需要从shell32.dll获取
		HMODULE hShell32DLL = ::LoadLibraryW(L"shell32.dll");
		if (hShell32DLL)
		{
			typedef HRESULT (WINAPI *pSelFun)( LPCITEMIDLIST pidlFolder, UINT cidl, LPCITEMIDLIST *apidl, DWORD dwFlags );
			pSelFun pFun = (pSelFun)::GetProcAddress(hShell32DLL, "SHOpenFolderAndSelectItems");
			::FreeLibrary(hShell32DLL);
			if(pFun != NULL)
			{ // 第二个参数cidl置为0，表示是选中文件
				bRet = SUCCEEDED(pFun(cpidl, 0, NULL, 0));
			}
		}
	} while (false);
	if (pDesktopFolder)
	{
		pDesktopFolder->Release();
	}
	::CoUninitialize();
	return bRet;
}

BOOL SelectFolderPath(CStringW& strFilePath)
{
	TCHAR szPathName[MAX_PATH] = { 0 };
	BROWSEINFO bInfo = { 0 };
	bInfo.hwndOwner = GetForegroundWindow(); // 父窗口;
	bInfo.lpszTitle = _T("选择目录");
	bInfo.ulFlags = BIF_RETURNONLYFSDIRS | BIF_USENEWUI /*包含一个编辑框 用户可以手动填写路径 对话框可以调整大小之类的..;*/
		| BIF_UAHINT; /*带TIPS提示*/ /*| BIF_NONEWFOLDERBUTTON 不带新建文件夹按钮*/ // 关于更多的 ulFlags 参考 http://msdn.microsoft.com/en-us/library/bb773205(v=vs.85).aspx;
	LPITEMIDLIST  lpDlist;
	lpDlist = SHBrowseForFolderW(&bInfo);
	if (nullptr == lpDlist) // 单击了确定按钮;
	{
		strFilePath.Empty();
		return FALSE;
	}
	SHGetPathFromIDListW(lpDlist, szPathName);
	strFilePath = szPathName;

	IMalloc * imalloc = 0;
	if (SUCCEEDED(SHGetMalloc(&imalloc)))
	{
		imalloc->Free(lpDlist);
		imalloc->Release();
	}
	return TRUE;
}

static WNDPROC g_lOriWndProc = NULL;
static CStringW g_usrSelectPath;
static HWND g_hMainWnd = NULL;
#ifndef ID_COMBO_ADDR 
	#define ID_COMBO_ADDR 0x47c
#endif // !#define ID_COMBO_ADDR 0x47c
#ifndef ID_LEFT_TOOBAR
	#define ID_LEFT_TOOBAR 0x4A0
#endif // !#define ID_LEFT_TOOBAR 0x4A0
static LRESULT WINAPI _FolderWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HWND g_hComboAddr = NULL;
	switch (uMsg)
	{
	case WM_COMMAND:
	{
		if (wParam == IDOK)
		{
			wchar_t wcDirPath[MAX_PATH] = { 0 };
			HWND hComboAddr = GetDlgItem(hwnd, ID_COMBO_ADDR);
			if (hComboAddr != NULL)
			{
				GetWindowText(hComboAddr, wcDirPath, MAX_PATH); g_hComboAddr = hComboAddr;
			}
			if (!wcslen(wcDirPath))
			{
				break;
			}
			DWORD dwAttr = GetFileAttributes(wcDirPath);
			if (dwAttr != -1 && (FILE_ATTRIBUTE_DIRECTORY & dwAttr))
			{
				LPOPENFILENAMEW oFn = (LPOPENFILENAME)GetProp(hwnd, L"OPENFILENAME");
				if (oFn)
				{
					if (_wcsicmp(g_usrSelectPath, wcDirPath) == 0)//用户自己选择的路径  不是自己粘贴到edit的路径  粘贴到edit的路径需要打开
					{
						int size = oFn->nMaxFile > MAX_PATH ? MAX_PATH : oFn->nMaxFile;
						memcpy(oFn->lpstrFile, wcDirPath, size * sizeof(wchar_t));
						RemoveProp(hwnd, L"OPENFILENAME");
						EndDialog(hwnd, IDOK);
					}
					else
					{
						g_usrSelectPath = wcDirPath;
					}
				}
				else
				{
					EndDialog(hwnd, IDCANCEL);
				}
			}
			break;
		}
		//////////////////////////////////////////////////////////////////////////
		//如果是左边toolbar发出的WM_COMMOND消息（即点击左边的toolbar）, 则清空OK按钮旁的组合框。
		HWND hCtrl = (HWND)lParam;
		if (hCtrl == NULL)
		{
			break;
		}
		int ctrlId = GetDlgCtrlID(hCtrl);
		if (ctrlId == ID_LEFT_TOOBAR)
		{
			HWND hComboAddr = GetDlgItem(hwnd, ID_COMBO_ADDR);
			if (hComboAddr != NULL)
			{
				SetWindowTextW(hComboAddr, L"");
			}
		}
	}
	break;
	}
	return  CallWindowProc((WNDPROC)g_lOriWndProc, hwnd, uMsg, wParam, lParam);
}

static UINT_PTR WINAPI FolderSelectProcCb(HWND hdlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	//参考reactos可知，hdlg 是一个隐藏的对话框，其父窗口为打开文件对话框， OK，CANCEL按钮等控件的消息在父窗口处理。
	if (uiMsg == WM_NOTIFY)
	{
		LPOFNOTIFY lpOfNotify = (LPOFNOTIFY)lParam;
		if (lpOfNotify->hdr.code == CDN_INITDONE)
		{
			if (g_hMainWnd)
			{//center window
				CRect rect;
				GetWindowRect(g_hMainWnd, &rect);
				::SetWindowPos(GetParent(hdlg), NULL, rect.left + (rect.Width() - 570) / 2, rect.top + (rect.Height() - 436) / 2, -1, -1, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE);			}
			SetWindowTextW(GetParent(hdlg), L"选择文件夹");
			SetDlgItemTextW(::GetParent(hdlg), 1, L"选择(&o)");
			//SetWindowPos(GetParent(hdlg), NULL, 0, 0, 900, 600, SWP_NOMOVE | SWP_NOZORDER); //里面的控件不会移动
			SetPropW(GetParent(hdlg), L"OPENFILENAME", (HANDLE)(lpOfNotify->lpOFN));
			g_lOriWndProc = (WNDPROC)::SetWindowLongPtr(::GetParent(hdlg), GWL_WNDPROC, (LONG)_FolderWndProc);
		}
		if (lpOfNotify->hdr.code == CDN_SELCHANGE)
		{
			wchar_t wcDirPath[MAX_PATH] = { 0 };
			CommDlg_OpenSave_GetFilePathW(GetParent(hdlg), wcDirPath, sizeof(wcDirPath));

			DWORD dwAttr = GetFileAttributes(wcDirPath);
			if (dwAttr == -1 || !(FILE_ATTRIBUTE_DIRECTORY & dwAttr))
			{
				wchar_t * lpLastSlash = wcsrchr(wcDirPath, L'\\');
				if (lpLastSlash)
				{
					wchar_t szNewPath[MAX_PATH];
					wcsncpy_s(szNewPath, wcDirPath, lpLastSlash - wcDirPath);
					DWORD dwAttr = GetFileAttributes(szNewPath);
					if (dwAttr != -1 && (FILE_ATTRIBUTE_DIRECTORY & dwAttr))
					{
						wcscpy_s(wcDirPath, MAX_PATH, szNewPath);
					}
				}
			}

			HWND hComboAddr = GetDlgItem(GetParent(hdlg), ID_COMBO_ADDR);
			if (hComboAddr != NULL)
			{
				if (wcslen(wcDirPath))
				{
					//去掉文件夹快捷方式的后缀名。
					int pathSize = wcslen(wcDirPath);
					if (pathSize >= 4)
					{
						wchar_t* wcExtension = PathFindExtensionW(wcDirPath);
						if (wcslen(wcExtension))
						{
							wcExtension = CharLowerW(wcExtension);
							if (!wcscmp(wcExtension, L".lnk"))
							{
								wcDirPath[pathSize - 4] = L'\0';
							}
						}
					}
					SetWindowTextW(hComboAddr, wcDirPath);
					g_usrSelectPath = wcDirPath;
				}
				else
				{
					SetWindowTextW(hComboAddr, L"");
					g_usrSelectPath.Empty();
				}
			}
		}
	}
	return TRUE;
}

BOOL SelectFolderPathEx(CStringW& strFilePath)
{
	wchar_t    szFileName[MAX_PATH] = { 0 };
	OPENFILENAME openFileName = { 0 };
	memset(&openFileName, 0, sizeof(openFileName));
	openFileName.lStructSize = sizeof(OPENFILENAME);
	openFileName.nMaxFile = MAX_PATH;

	GetCurrentDirectoryW(MAX_PATH, szFileName);
	if (!g_usrSelectPath.IsEmpty())
		wcscpy_s(szFileName, MAX_PATH, (LPCWSTR)g_usrSelectPath);
	openFileName.lpstrInitialDir = szFileName;//默认的文件路径 
	openFileName.lpstrFilter = L"文件夹\0..\0\0";
	openFileName.lpstrFile = szFileName; 
	openFileName.hwndOwner = GetForegroundWindow(); g_hMainWnd = GetForegroundWindow();
	openFileName.nFilterIndex = 1;
	openFileName.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_ENABLEHOOK | OFN_HIDEREADONLY;
	openFileName.hInstance = (HMODULE)GetCurrentProcess();
	openFileName.lpfnHook = FolderSelectProcCb;
	BOOL bRet = GetOpenFileNameW(&openFileName);
	if (bRet)
	{
		strFilePath = szFileName;
	}
	return bRet;
}

bool DlgImportImages(CStringW& filePath, CArray<CStringW>& images)
{
	bool mutiSelect = true;
	WCHAR szFilePath[2048] = { 0 };
	OPENFILENAME ofn = { 0 };
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = GetForegroundWindow();
	if (mutiSelect)
		ofn.lpstrFilter = L"Image files(*.png)\0*.png";
	else
		ofn.lpstrFilter = L"Image files(*.png, *.jpeg)\0*.png;*.jpeg;*.jpg\0";

	ofn.lpstrInitialDir = nullptr;//默认的文件路径   
	ofn.lpstrFile = szFilePath;//存放文件的缓冲区   
	ofn.nMaxFile = sizeof(szFilePath) / sizeof(*szFilePath);
	ofn.nFilterIndex = 0;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_EXPLORER; //标志如果是多选要加上OFN_ALLOWMULTISELECT 
	if (mutiSelect)
		ofn.Flags = ofn.Flags | OFN_ALLOWMULTISELECT;

	if (!GetOpenFileName(&ofn))
		return false;

	WCHAR szPath[MAX_PATH] = { 0 };
	lstrcpyn(szPath, szFilePath, ofn.nFileOffset);
	szPath[ofn.nFileOffset] = '\0';

	WCHAR* p = szFilePath + ofn.nFileOffset;
	filePath = szPath;
	while (*p)
	{
		WCHAR szFileName[MAX_PATH] = { 0 };
		lstrcat(szFileName, p);
		images.Add(szFileName);
		p += lstrlen(p) + 1;
	}
	return true;
}

bool CopyDirectory(CStringW strSrcDir,CStringW strDestDir)
{
	bool bRet = false;
	do 
	{
		if (strSrcDir.IsEmpty()||strDestDir.IsEmpty())		
		{
			break;
		}
		wchar_t szSrcFolder[MAX_PATH+1] = {0};
		wcscpy_s(szSrcFolder,strSrcDir.GetBuffer());strSrcDir.ReleaseBuffer();
		szSrcFolder[strSrcDir.GetLength()] =  L'\0';
		szSrcFolder[strSrcDir.GetLength()+1] = L'\0';

		wchar_t szDestFolder[MAX_PATH+1] = {0};
		wcscpy_s(szDestFolder,strDestDir.GetBuffer());strDestDir.ReleaseBuffer();
		szDestFolder[strDestDir.GetLength()] =  L'\0';
		szDestFolder[strDestDir.GetLength()+1] = L'\0';

		SHFILEOPSTRUCTW FileOp; 
		ZeroMemory(&FileOp, sizeof(SHFILEOPSTRUCT)); 
		FileOp.fFlags |= FOF_SILENT;        /*不显示进度*/
		FileOp.fFlags |= FOF_NOERRORUI ;    /*不报告错误信息*/
		FileOp.fFlags |= FOF_NOCONFIRMATION;/*不进行确认*/
		FileOp.hNameMappings = NULL;
		FileOp.hwnd = NULL;
	    FileOp.lpszProgressTitle = NULL;
		FileOp.wFunc = FO_COPY;
		FileOp.pFrom = szSrcFolder;        /*源目录，必须以2个\0结尾*/
		FileOp.pTo = szDestFolder;          /*目的目录，必须以2个\0结尾*/    
		if (0!= SHFileOperationW(&FileOp))
		{
			break;
		}

		bRet = true;
	} while (false);
	return bRet;
}

CStringW GetProgramDirectory()
{
	WCHAR szBuffer[MAX_PATH];
	memset(szBuffer, 0, sizeof szBuffer);
	::GetModuleFileName(NULL, szBuffer, MAX_PATH);
	CStringW Path;
	std::wstring path = szBuffer;
	std::wstring::size_type enpos = path.find_last_of(L'\\');
	if (enpos != std::wstring::npos)
		path = path.substr(0, enpos);

	Path = path.c_str();
	return Path;
}

// int DM_MessageBox(LPCWSTR lpText, UINT uType, LPCWSTR lpCaption,HWND hWnd)
// {
// 	DMSmartPtrT<MsgBox> pBox;pBox.Attach(new  MsgBox());
// 	if (NULL == hWnd)
// 	{
// 		hWnd = g_pMainWnd->m_hWnd;
// 	}
// 	int iRet = pBox->MessageBox(hWnd,lpText,lpCaption,uType);
// 	if (g_pMainWnd && g_pMainWnd->IsWindow())
// 	{
// 		g_pMainWnd->SetActiveWindow();
// 	}
// 	return iRet;
// }

int StringToInt(CStringW str)
{
	int iRet = 0;
	dm_parseint(str,iRet);
	return iRet;
}

CStringW IntToString(int id)
{
	CStringW str;
	str.Format(L"%d",id);
	return str;
}

CStringW FloatToString(float id)
{
	CStringW str;
	str.Format(L"%.2g", id);
	return str;
}

CStringW DoubleToString(double id)
{
	CStringW str;
	str.Format(L"%.2g", id);
	return str;
}

DMCode AutoDrawText(IDMCanvas*pCanvas,CStringW strFont,DMColor TextClr,LPCWSTR lpString, int nCount, LPRECT lpRect, UINT uFormat,BYTE alpha/*=0xFF*/)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		DMSmartPtrT<IDMRender> pRender;
		g_pDMApp->CreateRegObj((void**)&pRender,NULL, DMREG_Render);
		DMColor ClrOld = pCanvas->SetTextColor(TextClr);

		IDMFont* pFont = g_pDMApp->GetFont(strFont);
		DMSmartPtrT<IDMFont> pOldFont;
		pCanvas->SelectObject(pFont,(IDMMetaFile**)&pOldFont);

		iErr = pCanvas->DrawText(lpString,nCount,lpRect,uFormat,alpha);
		
		pCanvas->SetTextColor(ClrOld);
		pCanvas->SelectObject(pOldFont);
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode AutoDrawRoundRect(IDMCanvas*pCanvas,DMColor TextClr,int iStyle,int iWidth,LPCRECT lpRect,POINT &pt)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		DMSmartPtrT<IDMRender> pRender;
		g_pDMApp->CreateRegObj((void**)&pRender,NULL, DMREG_Render);
		DMSmartPtrT<IDMPen> pPen;
		pRender->CreatePen(TextClr,iStyle,iWidth, &pPen);

		DMSmartPtrT<IDMPen> pOldPen;
		pCanvas->SelectObject(pPen,(IDMMetaFile**)&pOldPen);;

		pCanvas->DrawRoundRect(lpRect,pt);
		
		pCanvas->SelectObject(pOldPen);	
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode AutoFillRoundRect(IDMCanvas*pCanvas,DMColor BrushClr,LPCRECT lpRect,POINT &pt)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		DMSmartPtrT<IDMRender> pRender;
		g_pDMApp->CreateRegObj((void**)&pRender,NULL, DMREG_Render);
		DMSmartPtrT<IDMBrush> pBrsh;
		pRender->CreateSolidColorBrush(BrushClr, &pBrsh);

		DMSmartPtrT<IDMBrush> pOldBrush;
		pCanvas->SelectObject(pBrsh,(IDMMetaFile**)&pOldBrush);;

		pCanvas->FillRoundRect(lpRect,pt);

		pCanvas->SelectObject(pOldBrush);	
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode AutoDrawDotLine(IDMCanvas*pCanvas, DMColor TextClr, int iStyle, int iWidth, LPPOINT lpPt, int cPoints)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (!lpPt)
			break;
		DMSmartPtrT<IDMRender> pRender;
		g_pDMApp->CreateRegObj((void**)&pRender, NULL, DMREG_Render);
		DMSmartPtrT<IDMPen> pPen;
		pRender->CreatePen(TextClr, iStyle, iWidth, &pPen);

		DMSmartPtrT<IDMPen> pOldPen;
		pCanvas->SelectObject(pPen, (IDMMetaFile**)&pOldPen);

		pCanvas->Polyline(lpPt, cPoints);

		pCanvas->SelectObject(pOldPen);
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DM::DMCode ListDirectory(const std::wstring& strFilePath, std::function<bool(const std::wstring&, const std::wstring&, bool)> callBack, bool recursie)
{
	bool             bSubdirectory = false;       // Flag, indicating whether
												 // subdirectories have been found
	HANDLE           hFile = INVALID_HANDLE_VALUE;// Handle to directory
	std::wstring     strPattern;                  // Pattern
	WIN32_FIND_DATAW FileInformation;             // File information

	std::wstring tmpDirPath = (strFilePath.back() == L'\\') ? strFilePath.substr(0, strFilePath.size() - 1) : strFilePath;
	strPattern = tmpDirPath + L"\\*.*";
	hFile = ::FindFirstFile(strPattern.c_str(), &FileInformation);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (0 != wcscmp(FileInformation.cFileName, L".") && 0 != wcscmp(FileInformation.cFileName, L".."))
			{
				std::wstring strDesPath = tmpDirPath + std::wstring(L"\\") + std::wstring(FileInformation.cFileName);
				bool isDir = ((FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY);
				if (callBack && callBack(strDesPath, FileInformation.cFileName, isDir) == false)
					return DM_ECODE_FAIL;

				if (isDir && recursie && ListDirectory(strDesPath, callBack, recursie) != DM_ECODE_OK)
					return DM_ECODE_FAIL;
			}
		} while (::FindNextFile(hFile, &FileInformation) == TRUE); 
	}
	return DM_ECODE_OK;
}

// DMAttrXml
DMCtrlXml::DMCtrlXml()
{
 	wchar_t szPath[MAX_PATH] = { 0 };

	// 初始化EDesigner.xml
	DM::GetRootFullPath(REC_FILE, szPath, MAX_PATH);
	if (!PathFileExists(szPath))
	{
		DMXmlNode NewNode = m_RecentDoc.Base();
		NewNode.InsertChildNode(L"Recently");
		m_RecentDoc.SaveXml(szPath);
	}
	else
	{
		m_RecentDoc.LoadFromFile(szPath);
	}

	// 初始化EDCrash.xml
	DM::GetRootFullPath(CRASH_FILE, szPath, MAX_PATH);
	if (PathFileExists(szPath))
	{
		m_CrashDoc.LoadFromFile(szPath);
	}
}

DMCtrlXml::~DMCtrlXml()
{
	wchar_t szPath[MAX_PATH] = { 0 };
	DM::GetRootFullPath(REC_FILE, szPath, MAX_PATH);
	m_RecentDoc.SaveXml(szPath);
}

bool DMCtrlXml::AddRecentResDir(CStringW strNewDir)
{
	bool bRet = false;
	do
	{
		//1.合法性判断
		if (strNewDir.IsEmpty() || !IsDirectoryExist(strNewDir))
		{
			break;
		}
		if (strNewDir.Right(1) != L"\\")
		{// 避免出现E:\MyRes和E:\MyRes\\并存的现象
			strNewDir += L"\\";
		}

		//2.循环查找是否已存在此dir记录
		DMXmlNode XmlRoot = m_RecentDoc.Root();
		DMXmlNode XmlExist, XmlLast;
		int iNum = 0;
		DMXmlNode XmlNode = XmlRoot.FirstChild();
		while (XmlNode.IsValid())
		{
			CStringW strDir = XmlNode.Attribute(XML_PATH);
			if (strDir.Right(1) != L"\\")
			{
				strDir += L"\\";
			}

			if (0 == strNewDir.CompareNoCase(strDir))
			{// 已存在
				XmlExist = XmlNode;
			}
			XmlLast = XmlNode;
			XmlNode = XmlNode.NextSibling();
			iNum++;
		}
		//3.限制iNum不能大于8条,否则移除最后一条,如果已存在此dir记录，iNum是不会变化的，所以判断限制为5，如果不存在，iNum将要+1,所以判断限制为4
		if (XmlExist.IsValid() && iNum > 8
			|| !XmlExist.IsValid() && iNum > 7
			)
		{
			XmlRoot.RemoveChildNode(&XmlLast);
		}

		//4.在最前面插入新记录
		if (XmlExist.IsValid())// 原来就存在了,先移除
		{
			XmlRoot.RemoveChildNode(&XmlExist);
		}

		DMXmlNode XmlFirst = XmlRoot.FirstChild();
		DMXmlNode XmlNewNode = XmlRoot.InsertChildNode(L"item", &XmlFirst, false);
		XmlNewNode.SetAttribute(XML_PATH, strNewDir);
		bRet = true;
	} while (false);
	return bRet;
}

bool DMCtrlXml::RemoveRecentResDir(CStringW strNewDir)
{
	bool bRet = false;
	do
	{
		DMXmlNode XmlRoot = m_RecentDoc.Root();
		DMXmlNode XmlNode = XmlRoot.FirstChild();
		while (XmlNode.IsValid())
		{
			CStringW strDir = XmlNode.Attribute(XML_PATH);
			if (0 == strNewDir.CompareNoCase(strDir))
			{
				XmlRoot.RemoveChildNode(&XmlNode);
				break;
			}
			XmlNode = XmlNode.NextSibling();
		}
		bRet = true;
	} while (false);
	return bRet;
}

bool DMCtrlXml::AddCrashBakProj(const CStringW& strOrgPath, const CStringW& strBackUpPath)
{
	bool bRet = false;
	do
	{
		//1.合法性判断
		if (strOrgPath.IsEmpty() || !IsDirectoryExist(strOrgPath) || strBackUpPath.IsEmpty() || !IsDirectoryExist(strBackUpPath))
		{
			break;
		}

		DMXmlNode NewNode = m_CrashDoc.Base();
		DMXmlNode nodexml;
		DMXmlNode XmlRoot = m_CrashDoc.Root(L"Crash");
		if (XmlRoot.IsValid())
		{
			nodexml = XmlRoot;
		}
		else
		{
			nodexml = NewNode.InsertChildNode(L"Crash");
		}
		nodexml.SetAttribute(XML_PATH, strOrgPath);
		nodexml.SetAttribute(L"CrashBak", strBackUpPath);

		wchar_t szPath[MAX_PATH] = { 0 };
		DM::GetRootFullPath(CRASH_FILE, szPath, MAX_PATH);
		m_CrashDoc.SaveXml(szPath);
		bRet = true;
	} while (false);
	return bRet;
}

void DMCtrlXml::GetCrashBakProj(CStringW& strOrgPath, CStringW& strBackUpPath)
{
	DMXmlNode XmlRoot = m_CrashDoc.Root(L"Crash");
	if (XmlRoot.IsValid())
	{
		CStringW strDir = XmlRoot.Attribute(XML_PATH);
		CStringW strCrashDir = XmlRoot.Attribute(L"CrashBak");
		if (!strDir.IsEmpty() && !strCrashDir.IsEmpty())
		{
			strOrgPath = strDir;
			strBackUpPath = strCrashDir;
			XmlRoot.SetAttribute(XML_PATH, L"");
			XmlRoot.SetAttribute(L"CrashBak", L"");

			wchar_t szPath[MAX_PATH] = { 0 };
			DM::GetRootFullPath(CRASH_FILE, szPath, MAX_PATH);
			m_CrashDoc.SaveXml(szPath);
		}
	}
}
