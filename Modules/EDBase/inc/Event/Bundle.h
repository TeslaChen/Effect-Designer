// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	Bundle.h
// File mark:   Event参数定义
// File summary:
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-6
// ----------------------------------------------------------------
#pragma once
#include "DMSmartPtrT.h"
#include "DMDelegate.h"

/// <summary>
///		单个数据封装
/// </summary>
class ED_EXPORT BundleData
{
public:
	enum{BUNDATA_NULL,BUNDATA_INT,BUNDATA_DOUBLE,BUNDATA_STRINGA,BUNDATA_STRINGW,BUNDATA_IUNKNOWN,BUNDATA_VOID};
	BundleData();
	BundleData(const BundleData &src);
	BundleData& operator=(const BundleData& src);
	~BundleData();
	bool setInt(int data);
	bool setDouble(double data);
	bool setStringA(LPCSTR data);
	bool setStringW(LPCWSTR data);
	bool setIUnknown(IUnknown* data);
	bool setVoidPtr(void* data);

	int getInt();
	double getDouble();
	LPCSTR getStringA();
	LPCWSTR getStringW();
	IUnknown* getIUnknown();					///<  注意,get后引用计数+1
	void* getVoidPtr();

public:
	int							 m_DataType;	///< 数据类型
	union 
	{
		int						 u_int;			///< 1
		double					 u_double;		///< 2
		LPCSTR					 u_stringA;		///< 3
		LPCWSTR					 u_stringW;		///< 4
		IUnknown*				 u_iunknown;	///< 5
		void*					 u_extra;		///< 6
	} m_Value;
};

/// <summary>
///		数据集
/// </summary>
class ED_EXPORT BundleImpl : public DMRefNum
{
public:
	void clear();
	int size();
	bool remove(LPCSTR key);
	bool hasKey(LPCSTR key);

	bool putInt(LPCSTR key, int data);
	bool putDouble(LPCSTR key, double data);
	bool putString(LPCSTR key, LPCSTR data);
	bool putStringW(LPCSTR key, LPCWSTR data);
	bool putIUnknownPtr(LPCSTR key, IUnknown* data);
	bool putVoidPtr(LPCSTR key, void* data);

	int getInt(LPCSTR key, int defData = 0);
	double getDouble(LPCSTR key, double defData = 0);
	LPCSTR getString(LPCSTR key, LPCSTR defData = NULL);
	LPCWSTR getStringW(LPCSTR key, LPCWSTR defData = NULL);
	IUnknown* getIUnknownPtr(LPCSTR key, IUnknown* defData = NULL);///< key 可以为NULL，调用者必须Release
	void* getVoidPtr(LPCSTR key, void* defData = NULL);

public:
	typedef std::map<std::string, BundleData>		t_mapKeyValue;
	t_mapKeyValue								    m_MapKeyValue;
};

/// <summary>
///		数据集封装层
/// </summary>
class ED_EXPORT Bundle
{
public:
	Bundle();
	Bundle(const Bundle& src);
	Bundle& operator=(const Bundle& src);
	~Bundle();

public:
	Bundle clone();
	void clear();
	int size() const; 
	bool remove(LPCSTR key);
	bool hasKey(LPCSTR key) const;

	bool putInt(LPCSTR key, int data);
	bool putDouble(LPCSTR key, double data);
	bool putString(LPCSTR key, LPCSTR data);
	bool putStringW(LPCSTR key, LPCWSTR data);
	bool putUTF8String(LPCSTR key, LPCSTR data);
	bool putIUnknownPtr(LPCSTR key, IUnknown* data);
	bool putVoidPtr(LPCSTR key, void* data);
	
	int getInt(LPCSTR key, int defData = 0) const;
	double getDouble(LPCSTR key, double defData = 0) const;
	LPCSTR getString(LPCSTR key, LPCSTR defData = "") const;
	LPCWSTR getStringW(LPCSTR key, LPCWSTR defData = L"") const;
	LPCSTR getUTF8String(LPCSTR key, LPCSTR defData = "") const;
	IUnknown* getIUnknownPtr(LPCSTR key, IUnknown* defData = NULL) const;
	void* getVoidPtr(LPCSTR key, void* defData = NULL) const;
public:
	DM::DMSmartPtrT<BundleImpl>					   m_pBundleImpl;		///< Bundle在传递时，m_pBundleImpl自动+1，从而保证BundleImpl的生命周期				
};	

/// <summary>
///		发送方
/// </summary>
enum EventSenderID{EventSenderID_Unknown,EventSenderID_Base};
class ED_EXPORT EventSender
{
public:
	EventSender():m_dwSize(sizeof(EventSender)),m_dwId(EventSenderID_Unknown){}
public:
	DWORD                                         m_dwSize;
	DWORD                                         m_dwId;
};


/// <summary>
///		事件槽bool function(const YGEventSender& , const YGBundle&)函数进行抽象
/// </summary>
typedef DM::DMDelegate< bool(const EventSender& sender, const Bundle& evt) > GPSlot;