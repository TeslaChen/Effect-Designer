// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CodeHelper.h 
// File mark:   
// File summary:copy form yg_coder.h 
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-1
// ----------------------------------------------------------------
#pragma once

namespace EDBASE
{
/// <summary>
///		string和wstring互转及相关函数
/// </summary>

//#define  ED_EXPORT

class a2w;class w2a;
#define w2u(strWide)	w2a(strWide, CP_UTF8)
#define u2w(strUtf8)	a2w(strUtf8, CP_UTF8)
#define a2u(strAnsi)	w2a(EDBASE::a2w(strAnsi), CP_UTF8)
#define u2a(strUtf8)	w2a(EDBASE::a2w(strUtf8), CP_UTF8))
	class ED_EXPORT a2w
	{
	public:
		a2w(LPCSTR psz, UINT codePage=CP_ACP) throw(...);
		a2w(const std::string& str, UINT codePage=CP_ACP) throw(...);
		~a2w() throw();
		operator std::wstring() const throw(){return m_lpsz ? m_lpsz : std::wstring();}

	private:
		void Init(LPCSTR psz, UINT codePage) throw(...);
	
	private:
		LPWSTR						m_lpsz;
		wchar_t						m_szbuffer[128];

	private:
		a2w(const a2w&) throw();
		a2w& operator=(const a2w&) throw();
	};

	class ED_EXPORT w2a
	{
	public:
		w2a(LPCWSTR psz, UINT codePage=CP_ACP) throw(...);
		w2a(const std::wstring& str, UINT codePage=CP_ACP) throw(...);
		~w2a() throw();
		operator std::string() const throw(){return m_lpsz ? m_lpsz : std::string();}

	private:
		void Init(LPCWSTR psz, UINT codePage) throw(...);
	
	private:
		LPSTR						m_lpsz;
		char						m_szbuffer[128];

	private:
		w2a(const w2a&) throw();
		w2a& operator=(const w2a&) throw();
	};

	ED_EXPORT int Format(std::wstring& str, IN const wchar_t* szFormat, ...);
	ED_EXPORT int Format(std::string& str, IN const char* szFormat, ...);
	ED_EXPORT int CompareNoCase(const std::wstring& str1, const std::wstring& str2);
	ED_EXPORT int CompareNoCase(const std::string& str1, const std::string& str2);

	ED_EXPORT std::wstring& ToLower(std::wstring& str);
	ED_EXPORT std::string& ToLower(std::string& str);
	ED_EXPORT std::wstring& ToUpper(std::wstring& str);
	ED_EXPORT std::string& ToUpper(std::string& str);

/// <summary>
///		常用字符串转换函数
/// </summary>
	ED_EXPORT std::string Bytes2Hex(const void* digest,int ilen);
	ED_EXPORT std::string Hex2Bytes(const char* pszInput);

	ED_EXPORT std::string Base64Encode(const unsigned char* pbIn,unsigned int sizeIn);
	ED_EXPORT int Base64Decode(const std::string& str_in,unsigned char* pbOut,int sizeOut);
	ED_EXPORT std::string Base64Encode(const std::string& str_in);
	ED_EXPORT std::string Base64Decode(const std::string& str_in);
	ED_EXPORT std::wstring Base64WEncode(const std::wstring& str_in);
	ED_EXPORT std::wstring Base64WDecode(const std::wstring& str_in);

	ED_EXPORT int __stdcall Des3Encode(unsigned char *pbIn, unsigned int size, unsigned char* pwd, unsigned char* pbOut);
	ED_EXPORT int __stdcall Des3Decode(unsigned char *pbIn, unsigned int size, unsigned char* pwd, unsigned char* pbOut);

	ED_EXPORT std::string  URLEncode(LPCSTR pszUrl);
	ED_EXPORT std::string  URLEncode(const std::string& str_in);
	ED_EXPORT std::wstring URLEncode(LPCWSTR pszUrl);
	ED_EXPORT std::string  URLEncode(const std::wstring& str_in);
	ED_EXPORT std::string  URLDecode(const std::string& str_in);
	ED_EXPORT void UnicodeDecode(std::wstring& str_out, LPCSTR pbIn, size_t nSize);
	ED_EXPORT UINT GetHashKey(const unsigned char* pbKey, UINT nSizeKey);
	ED_EXPORT std::wstring GetGUID();
	ED_EXPORT std::vector<std::wstring> split_string(std::wstring &szInput, std::wstring &szToken);

	ED_EXPORT std::string MD5String(const std::string& str);
	ED_EXPORT std::wstring MD5String(const std::wstring& str);
	ED_EXPORT std::wstring MD5File(const std::wstring& file_name);
	ED_EXPORT std::string MD5File(const std::string& file_name);

	// 
	ED_EXPORT std::wstring GetCookieString(std::map<std::wstring, std::wstring>&strMap);
}