#include "EDBaseAfx.h"
#include "CodeHelper.h"
#include "Base64.h"
#include "DES3.h"
#include "md5.h"


namespace EDBASE
{
	/// a2w ---------------------------------------------------------------
	a2w::a2w(LPCSTR psz, UINT codePage/*=CP_ACP*/) throw(...) : m_lpsz(m_szbuffer)
	{
		Init(psz, codePage);
	}

	a2w::a2w(const std::string& str, UINT codePage/*=CP_ACP*/) throw(...) : m_lpsz(m_szbuffer)
	{
		Init(str.c_str(), codePage);
	}

	a2w::~a2w() throw()
	{
		if (m_lpsz && m_lpsz != m_szbuffer)
		{
			delete[] m_lpsz;
			m_lpsz = NULL;
		}
	}

	void a2w::Init(LPCSTR psz, UINT codePage) throw(...)
	{
		if (psz == NULL)
		{
			m_lpsz = NULL;
			return;
		}
		int wlen = ::MultiByteToWideChar(codePage, 0, psz, -1, m_lpsz, 128); // 尝试直接转换，wlen包含空结束符
		if (wlen == 0 && GetLastError()==ERROR_INSUFFICIENT_BUFFER) // 转换失败，缓冲区不够
		{
			wlen = ::MultiByteToWideChar(codePage, 0, psz, -1, NULL, 0); // 获取需要的缓冲区大小
			m_lpsz = new wchar_t[wlen];
			wlen = ::MultiByteToWideChar(codePage, 0, psz, -1, m_lpsz, wlen);
		}

		if (wlen == 0) // failed
			::RaiseException((DWORD)EXCEPTION_ILLEGAL_INSTRUCTION, (DWORD)EXCEPTION_NONCONTINUABLE, 0, NULL );
	}


	/// w2a ---------------------------------------------------------------
	w2a::w2a(LPCWSTR psz, UINT codePage/*=CP_ACP*/) throw(...) : m_lpsz(m_szbuffer)
	{
		Init(psz, codePage);
	}

	w2a::w2a(const std::wstring& str, UINT codePage/*=CP_ACP*/) throw(...) : m_lpsz(m_szbuffer)
	{
		Init(str.c_str(), codePage);
	}

	w2a::~w2a() throw()
	{
		if (m_lpsz && m_lpsz != m_szbuffer)
		{
			delete[] m_lpsz;
			m_lpsz = NULL;
		}
	}

	void w2a::Init(LPCWSTR psz, UINT codePage) throw(...)
	{
		if (psz == NULL)
		{
			m_lpsz = NULL;
			return;
		}
		int len = ::WideCharToMultiByte(codePage, 0, psz, -1, m_lpsz, 128, NULL, NULL); // 尝试直接转换，wlen包含空结束符
		if (len == 0 && GetLastError()==ERROR_INSUFFICIENT_BUFFER) // 转换失败，缓冲区不够
		{
			len = ::WideCharToMultiByte(codePage, 0, psz, -1, NULL, 0, NULL, NULL); // 获取需要的缓冲区大小
			m_lpsz = new char[len];
			len = ::WideCharToMultiByte(codePage, 0, psz, -1, m_lpsz, len, NULL, NULL);
		}

		if (len == 0) // failed
			::RaiseException((DWORD)EXCEPTION_ILLEGAL_INSTRUCTION, (DWORD)EXCEPTION_NONCONTINUABLE, 0, NULL );
	}

//------------------------------------------------------------------------------------------------------
	char Num2Char(const long int NumIn)
	{
		if (NumIn < 10) return (char)(NumIn+'0');	 //如输入小于10则直接返回
		else			return (char)(NumIn-10+'a'); //如输入大于9则用字母表示，如'A'表示10，'Z'表示35
	}
	int Char2Num(const char CharIn)
	{
		if(CharIn>='a')		 return (CharIn-'a'+10);
		else if (CharIn<'A') return (CharIn-'0');
		else				 return (CharIn-'A'+10);
	}
	std::string Bytes2Hex(const void* digest,int ilen)
	{
		std::string sHex;
		register unsigned char* pb = (unsigned char*)digest;
		for (int i=0; i<ilen; i++)
		{
			sHex += Num2Char(pb[i]>>4);
			sHex += Num2Char(pb[i]&0xF);
		}
		return sHex;
	}

	std::string Hex2Bytes(const char* pszInput)
	{
		std::string sRet;
		if (NULL == pszInput) return sRet;
		int nSize = strlen(pszInput) / 2;
		unsigned char ch;
		INT nPos;
		for (int i=0; i<nSize; i++)
		{
			nPos = i * 2;
			ch = Char2Num(pszInput[nPos]) << 4;
			ch += Char2Num(pszInput[nPos + 1]);
			sRet.push_back(ch);
		}
		return sRet;
	}

	std::string Base64Encode(const unsigned char* pbIn,unsigned int sizeIn)
	{
		std::string sRet;
		char *scOut = NULL;
		int iout = (((sizeIn+2)/3)<<2);
		scOut = new char[iout];
		memset(scOut,0,iout);
		Base64Coder  coder;
		int nCount = coder.Encode((char*)pbIn,sizeIn,scOut);
		if (scOut!=NULL)
		{
			sRet = std::string(scOut,nCount);
			delete[] scOut;
			scOut=NULL;
		}
		return sRet;
	}

	int Base64Decode(const std::string& str_in,unsigned char* pbOut,int sizeOut)
	{
		int iLen = str_in.size();
		int idec = (iLen>>2)*3;
		if (idec>sizeOut || pbOut==NULL) return 0;
		Base64Coder  mtEnc;
		idec = mtEnc.Decode(str_in.c_str(),iLen,(char*)pbOut);
		return idec;
	}

	std::string Base64Encode(const std::string& str_in)
	{
		return Base64Encode((unsigned char*)str_in.c_str(),str_in.size());
	}

	std::string Base64Decode(const std::string& str_in)
	{
		int idec = (str_in.size()>>2)*3;
		unsigned char* vout = new unsigned char[idec];
		memset(vout,0,idec);
		idec = Base64Decode(str_in,vout,idec);
		std::string ret;
		if (idec>0) ret = std::string((char*)vout,idec);
		delete[] vout;	vout = NULL;
		return ret;
	}

	std::wstring Base64WEncode(const std::wstring& str_in)
	{
		std::string str = Base64Encode((unsigned char*)str_in.c_str(),str_in.size()*sizeof(wchar_t));
		std::wstring wstr(str.begin(),str.end());
		return wstr;
	}

	std::wstring Base64WDecode(const std::wstring& str_in)
	{
		std::string str_in2(str_in.begin(),str_in.end());
		int idec = (str_in.size()>>2)*3;
		unsigned char* vout = new unsigned char[idec];
		memset(vout,0,idec);
		idec = Base64Decode(str_in2,vout,idec);
		std::wstring ret;
		if(idec>0) ret = std::wstring((wchar_t*)vout);
		delete[] vout;	vout = NULL;
		return ret;
	}

	int __stdcall Des3Encode(unsigned char *pbIn, unsigned int size, unsigned char* pwd, unsigned char* pbOut)
	{
		int ret=0, pos=0, iCount, i;
		if(pbIn==0 || pwd==0 || pbOut==pbIn) return ret;
		iCount=(size+7)/8;
		ret=iCount<<3;
		memset(pbOut,0,ret);
		memcpy(pbOut,pbIn,size);
		des3key(pwd,EN0);
		for (i=0;i<iCount;i++)
		{
			Ddes(pbOut+pos,pbOut+pos);
			pos+=8;
		}
		return ret;
	}

	int __stdcall Des3Decode(unsigned char *pbIn, unsigned int size, unsigned char* pwd, unsigned char* pbOut)
	{  
		int ret=0, pos=0, iCount, i;
		if(pbIn==0 || pwd==0 || pbOut==pbIn) return ret;
		iCount=(size+7)/8;
		ret=iCount<<3;
		memset(pbOut,0,ret);
		memcpy(pbOut,pbIn,size);
		des3key(pwd,DE1);
		for(i=0;i<iCount;i++)
		{
			Ddes(pbOut+pos,pbOut+pos);
			pos+=8;
		}
		return ret;
	}

	std::string URLEncode(LPCSTR pszUrl)
	{
		static char hex[] = "0123456789ABCDEF";  
		std::string dst;  
		if (!IsValidString(pszUrl)) return dst;

		size_t nSize = strlen(pszUrl);
		for (size_t i = 0; i < nSize && pszUrl[i] != 0; i++)  
		{  
			unsigned char ch = pszUrl[i];  
			if (isalnum(ch) || ch == '.')  
			{  
				dst += ch;  
			}  
			else
			{  
				unsigned char c = static_cast<unsigned char>(pszUrl[i]);  
				dst += '%';  
				dst += hex[c >> 4];  
				dst += hex[c % 16];  
			}  
		}  
		return dst; 
	}

	std::string URLEncode(const std::string& str_in)
	{
		return URLEncode(str_in.c_str());
	}

	std::wstring URLEncode(LPCWSTR pszUrl)
	{
		if (IsValidString(pszUrl)) 
		{
			std::string strUTF8 = w2a(pszUrl,CP_UTF8);
			std::wstring strW = a2w(URLEncode(strUTF8.c_str()));
			return strW;
		}
		return std::wstring();
	}

	std::string URLEncode(const std::wstring& str_in)
	{
		std::string sUtf8 = w2a(str_in,CP_UTF8);
		return URLEncode(sUtf8.c_str());
	}

#define IsHexNum(c) ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f'))
	std::string URLDecode(const std::string& str_in)
	{
		int nSize  = str_in.size();
		char* pUrl = new char[nSize+1];
		ZeroMemory(pUrl,nSize+1);
		strcpy(pUrl,str_in.c_str());
		int i = 0;
		int length = (int)strlen(pUrl);
		CHAR *pBuf = new CHAR[length+1];
		ZeroMemory(pBuf, length+1);
		LPSTR p = pBuf;
		//char chv;
		while(i < length)
		{
			if(i <= length -3 && pUrl[i] == '%' && IsHexNum(pUrl[i+1]) && IsHexNum(pUrl[i+2]))
			{
				sscanf(pUrl + i + 1, "%2x", p++);
				i += 3;
			}
			else
			{
				*(p++) = pUrl[i++];
			}
		}

		std::string strUTF8(pBuf);
		delete [] pUrl;
		delete [] pBuf;
		return strUTF8;
	}

	void UnicodeDecode(std::wstring& str_out, LPCSTR pbIn, size_t nSize)
	{
		wchar_t t = 0;
		CStringW str;
		bool  bRet = false;
		for (size_t i = 0; i < nSize; ++i)
		{
			switch(pbIn[i])
			{
			case '\\':
				if (i < nSize && pbIn[i + 1] == 'u') 
				{
					if (bRet && t) 
					{
						str.AppendChar(t);
					}
					bRet = true;
					t = 0;
				}
				else
				{
					bRet = false;
					if (t)
						str.AppendFormat(L"%c", t);
				}
				break;
			case 'u':
				if (bRet) 
					continue;
				else if (t)
					str.AppendFormat(L"%c", t);
				else 
					str.AppendFormat(L"%c", (wchar_t)pbIn[i]);
				break;
			default:
				if (bRet)
				{
					if (( (pbIn[i] >= '0' && pbIn[i] <= '9' ) || ( pbIn[i] >= 'a' && pbIn[i] <= 'f' ) ||
						(pbIn[i] >= 'A' && pbIn[i] <= 'F' ) ) && t < 0xFFF)
					{
						t *= 16;
						if (pbIn[i] >= '0' && pbIn[i] <= '9' )
							t += pbIn[i] - '0';
						else if(( pbIn[i] >= 'a' && pbIn[i] <= 'f' ))
							t += pbIn[i] - 'a'+ 10;
						else if(pbIn[i] >= 'A' && pbIn[i] <= 'F')
							t += pbIn[i] - 'A' + 10;
					}
					else
					{
						if(t)
							str.AppendChar(t);
						str.AppendFormat(L"%c", (wchar_t)pbIn[i]);
						t = 0;
						bRet = false;
					}
				}
				else
				{
					str.AppendFormat(L"%c", (wchar_t)pbIn[i]);
					t = 0;
				}
				break;
			}
		}
		if(t)
			str.AppendChar(t);
		// out
		str_out = str.GetBuffer();str.ReleaseBuffer();
	}

	UINT GetHashKey(const unsigned char* pbKey, UINT nSizeKey)
	{
		unsigned char* pbStart = const_cast<unsigned char*>(pbKey);
		const unsigned char* pbEnd = pbStart + nSizeKey;
		UINT nHash;
		for (nHash = 0; pbStart < pbEnd; pbStart++)
		{
			nHash *= 16777619;
			nHash ^= (UINT)(*pbStart);
		}
		return nHash;
	}

	std::wstring GetGUID()
	{
		std::wstring strGUID;
		TCHAR szGUID[MAX_PATH] ={0};
		GUID guid = {0};
		HRESULT hr = CoCreateGuid( &guid );
		if( S_OK == hr ) {
			if(StringFromGUID2(guid, szGUID, MAX_PATH) > 0) {
				strGUID = szGUID;
				int nStarPoint = strGUID.find(_T("{")) + 1;
				int nLength = strGUID.rfind(_T("}")) - 1;

				if(nStarPoint > 0 && nLength > 0) {
					strGUID = strGUID.substr(nStarPoint, nLength);
				} else {
					strGUID = _T("");
				}
			}
		}
		return strGUID;
	}

	///-------------------------------------------------------------------------------------------------
	int FormatV(std::wstring& str,IN const wchar_t* szFormat, IN va_list args)
	{
		int nNeedLen = 0;
		

		nNeedLen = _vscwprintf_p(szFormat, args) + 1;
		//强制分配内存。
		DMBufT<wchar_t> pszbuffer;pszbuffer.Allocate(nNeedLen);
		vswprintf_s(pszbuffer, nNeedLen, szFormat, args);
		str = pszbuffer;// str是深拷贝

		return true;
	}

	int FormatV(std::string& str,IN const char* szFormat, IN va_list args)
	{
		int nNeedLen = 0;
		std::string strBuffer;

		nNeedLen = _vscprintf_p(szFormat, args) + 1;
		//强制分配内存。
		DMBufT<char> pszbuffer;pszbuffer.Allocate(nNeedLen);
		vsprintf_s(pszbuffer, nNeedLen, szFormat, args);
		str = pszbuffer;// str是深拷贝

		return true;
	}

	int Format(std::wstring& str, IN const wchar_t* szFormat, ...)
	{
		int nResult = false;
		va_list args;
		va_start(args, szFormat);
		nResult = FormatV(str,szFormat, args);
		va_end(args);

		return nResult;
	}

	int Format(std::string& str, IN const char* szFormat, ...)
	{
		int nResult = false;
		va_list args;
		va_start(args, szFormat);
		nResult = FormatV(str,szFormat, args);
		va_end(args);

		return nResult;
	}
	
	std::wstring& ToLower(std::wstring& str)
	{	// 把字符串全部转换为小写
		std::transform(str.begin(), str.end(), str.begin(), ::towlower);
		return str;
	}

	std::string& ToLower(std::string& str)
	{	// 把字符串全部转换为小写
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);
		return str;
	}

	std::wstring& ToUpper(std::wstring& str)
	{
		std::transform(str.begin(), str.end(), str.begin(), ::towupper);
		return str;
	}

	std::string& ToUpper(std::string& str)
	{
		std::transform(str.begin(), str.end(), str.begin(), ::toupper);
		return str;
	}

	int CompareNoCase(const std::wstring& str1, const std::wstring& str2)
	{
		std::wstring s1(str1), s2(str2);
		return ToLower(s1).compare(ToLower(s2));
	}

	int CompareNoCase(const std::string& str1, const char* str2)
	{
		std::string s1(str1), s2(str2);
		return ToLower(s1).compare(ToLower(s2));
	}

	std::vector<std::wstring> split_string(std::wstring &szInput, std::wstring &szToken)
	{
		std::vector<std::wstring> result;
		int pos = 0;
		int begin = 0;
		const int size = szToken.size();
		while (true)
		{
			if ((pos=szInput.find(szToken,begin))==std::wstring::npos)
			{
				result.push_back(szInput.substr(begin));
				return result;
			}
			result.push_back(szInput.substr(begin, pos-begin));
			begin = pos + size;
		}
	}

	std::string MD5String(const std::string& str) {
		md5_state_s state;
		md5_byte_t digest[16];
		char hex_output[16*2 + 1];

		md5_init(&state);
		md5_append(&state, (const md5_byte_t*)str.c_str(), str.length());
		md5_finish(&state, digest);
		for (int di = 0; di < 16; ++di) 
		{
			sprintf(hex_output + di * 2, "%02x", digest[di]);
		}
		_strupr_s(hex_output);
		return hex_output;

	}
	std::wstring MD5String(const std::wstring& str) {
		std::wstring szMd5	= a2w( MD5String(w2a(str)) );
		return szMd5;
	}


	std::wstring MD5File(const std::wstring& file_name) 
	{
		HANDLE hfile = CreateFile(file_name.c_str(), GENERIC_READ, FILE_SHARE_READ,
			NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (INVALID_HANDLE_VALUE == hfile) {
			return L"";
		}
		md5_state_s state;
		md5_byte_t digest[16];
		wchar_t hex_output[16*2 + 1];
		char* buffer		= NULL;

		LARGE_INTEGER dwSize = {0};
		GetFileSizeEx(hfile, &dwSize);

		DWORD dwMaxSize		= 1024 * 1024 * 10;
		if (dwSize.LowPart < dwMaxSize)
		{
			dwMaxSize = dwSize.LowPart;
		}
		buffer				= new char[dwMaxSize];
		memset(buffer, 0, dwMaxSize);

		md5_init(&state);

		DWORD dwread;
		while (0 != ReadFile(hfile, buffer, dwMaxSize, &dwread, NULL) && 0 < dwread) {
			md5_append(&state, (const md5_byte_t*)buffer, dwread);
			Sleep(1);
		}
		CloseHandle(hfile);
		md5_finish(&state, digest);
		for (int di = 0; di < 16; ++di) {
			swprintf(hex_output + di * 2, L"%02x", digest[di]);
		}
		delete [] buffer;
		_wcsupr_s(hex_output);
		return hex_output;
	}
	std::string MD5File(const std::string& file_name) 
	{
		std::string szMd5	= w2a(MD5File(a2w(file_name)));
		return szMd5;	
	}

	std::wstring GetCookieString(std::map<std::wstring, std::wstring>&strMap)
	{
		std::wstring sz_value;
		std::map<std::wstring, std::wstring>::iterator it;
		for (it=strMap.begin();it!=strMap.end();++it)
		{
			sz_value += it->first;
			sz_value += L'=';
			sz_value += it->second;
			sz_value += L';';
		}
		return sz_value;
	}
}

