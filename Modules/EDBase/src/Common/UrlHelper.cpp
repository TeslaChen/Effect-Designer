#include "EDBaseAfx.h"
#include "UrlHelper.h"
#include <wininet.h>
#pragma comment(lib, "Wininet.lib")

namespace UrlHelper
{
	bool isSpace(wchar_t s)
	{
		return (s == L' ')?true:false;
	}

	bool IsValidUrl(LPCWSTR lpszUrl)
	{
		static LPCWSTR szUrlFilterZone[] = {L"https://", L"http://"};
		bool bRet = false;
		if (IsValidString(lpszUrl))
		{
			int nCount = sizeof(szUrlFilterZone) / sizeof(szUrlFilterZone[0]);
			for (int i = 0; i< nCount; i ++)
			{
				if (0 == _wcsnicmp(lpszUrl, szUrlFilterZone[i], wcslen(szUrlFilterZone[i])))
				{
					bRet = true;
					break;
				}
			}
		}
		return bRet;
	}

	bool IsDownloadUrl(LPCWSTR lpszUrl)
	{
		static LPCWSTR szFileExt[] = {L".zip", L".7z", L".rar", L".cab", L".exe", L".msi", L".doc", L".pdf", L".bat", L".ocx", L".crt" };
		bool bRet = false;
		if (IsValidString(lpszUrl))
		{
			LPCWSTR pszExt = wcsrchr(lpszUrl, L'.');
			if (IsValidString(pszExt))
			{
				int nCount = sizeof(szFileExt) / sizeof(szFileExt[0]);
				for (int i = 0; i< nCount; i ++)
				{
					if (_wcsicmp(szFileExt[i], pszExt) == 0)
					{
						bRet = true;
						break;
					}
				}
			}
		}
		return bRet;
	}

	bool SetCookieForMainDomain(LPCWSTR pszCookie)
	{
		bool bRet =false;
		LPCWSTR szDomain[] =
		{
			L"http://duowan.com/",
			L"http://yy.com/",
			L"http://4366.com/",
			L"http://.4366.com",
			L"http://5153.com/",
			L"http://5253.com/"
		};
		int nCount = sizeof(szDomain) / sizeof(szDomain[0]);
		for (int i=0; i< nCount; i++)
		{
			bRet |= SetCookieForUrl(szDomain[i], pszCookie);
		}
		return bRet;
	}

	bool SetCookieForUrl(LPCWSTR lpszUrl, LPCWSTR lpszCookie)
	{// 游戏不调用！
		if (false == IsValidString(lpszCookie) || false == IsValidString(lpszUrl))
			return false;

		std::wstring _strCookie = lpszCookie;
		_strCookie.erase(std::remove_if(_strCookie.begin(), _strCookie.end(), isSpace),_strCookie.end());
		int _nStart = 0;
		int nCount = 0;
		do 
		{
			std::wstring::size_type _nEnd = _strCookie.find(L'=', _nStart);
			if (std::wstring::npos != _nEnd)
			{
				std::wstring _strPrev = _strCookie.substr(_nStart, _nEnd - _nStart);
				std::wstring::size_type _nPrevEnd = _strPrev.find(L';', 0); // 出现了project;的现象
				if (_nPrevEnd != std::wstring::npos) 
				{
					++_nPrevEnd;
					_strPrev = _strPrev.substr(_nPrevEnd, _strPrev.length() - _nPrevEnd);
				}

				_nStart = _nEnd + 1;
				_nEnd = _strCookie.find(L';', _nStart);
				std::wstring _strEnd = (_strCookie.substr(_nStart, (-1 == _nEnd) ? (_strCookie.length() - _nStart) : _nEnd - _nStart));
				if (InternetSetCookieEx(lpszUrl, _strPrev.c_str(), _strEnd.c_str(), INTERNET_COOKIE_EVALUATE_P3P,(DWORD_PTR)_T("CP=\"CURa CAO PSA OUR OTC DSP COR\"")) != FALSE)
				{
					nCount++;
				}
				
			}
			_nStart = (std::wstring::npos != _nEnd) ? _nEnd + 1 : _nEnd;
		} while (_nStart != std::wstring::npos);
		return nCount != 0;
	}

	bool IsPageTitleValid(LPCWSTR lpszTitle)
	{
		bool bRet = false;
		if (IsValidString(lpszTitle) &&0 != _wcsicmp(L"#", lpszTitle))
		{
			bRet = true;
		}
		return bRet;
	}

	bool FormatUrl(std::wstring& strRedirect,std::wstring& strGameId, std::wstring& strServerId, std::wstring& strPassport, std::wstring& strRso,std::wstring& strRef,DMOUT std::wstring& strGameUrl)
	{
		bool bRet = false;
		do 
		{
			if (strRedirect.empty() ||  strGameId.empty())
			{
				break;
			}
			#define GP_STR_FORMAT_GAME_REDIRECT L"%s?game=%s&server=%s&ref=%s&from=%s&rso=%s&pro=4366Game"
			EDBASE::Format(strGameUrl,
				GP_STR_FORMAT_GAME_REDIRECT,
				strRedirect.c_str(),
				strGameId.c_str(),
				strServerId.c_str(),
				strRef.c_str(),
				strRso.c_str(),
				strRso.c_str());

			bRet = true;
		} while (false);
		return bRet;
	}

	std::wstring FormatUrlTags(std::wstring& strGameId, std::wstring& strServerId, std::wstring& strPassPort)
	{
		std::wstring strTags;
		do 
		{
			if (strGameId.empty()&&strServerId.empty()&&strPassPort.empty())
			{
				break;
			}

			strTags = strGameId + L"@3@2@1@";
			strTags += strServerId;
			strTags += L"@3@2@1@";
			strTags += strPassPort;
		} while (false);
		return EDBASE::ToLower(strTags);
	}

	bool UnFormatUrlTags(std::wstring& strTags,std::wstring& strGameId, std::wstring& strServerId, std::wstring& strPassPort)
	{
		bool bRet = false;
		do 
		{
			if (strTags.empty())
			{
				break;
			}
			std::vector<std::wstring> vecParam = EDBASE::split_string(strTags, std::wstring(L"@3@2@1@"));//解析参数
			if (vecParam.size()!=3)
			{
				break;
			}
			strGameId = vecParam[0];
			strServerId = vecParam[1];
			strPassPort = vecParam[2];
			bRet = true;
		} while (false);
		return bRet;
	}

	std::wstring GetDomainForUrl(std::wstring strUrl)
	{
		std::wstring strRet = strUrl;
		std::wstring strHttp; 
		signed __int32 nPos ;
		nPos = strUrl.find_first_of(L"://");
		if (nPos != -1)
		{
			strHttp = strUrl.substr(0,nPos+wcslen(L"://"));
			strUrl = strUrl.substr(nPos + _tcslen(L"://"));
		}
		nPos = strUrl.find_first_of(L"/");
		if (nPos != -1)
		{
			strRet = strHttp;
			strRet += strUrl.substr(0,nPos);
		}
		return strRet;
	}
}
